import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Componentes
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AcercaComponent } from './acerca/acerca.component';
import { ListaComponent } from './lista/lista.component';
import { MostrarComponent } from './mostrar/mostrar.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { EditarComponent } from './editar/editar.component';

// Servicios
import { UsersService } from './services/users.service';

// Rutas
const routes: Routes = [
  {
    path: 'usuarios',
    component: ListaComponent
  },
  {
    path: 'usuarios-detalles/:id',
    component: MostrarComponent
  },
  {
    path: 'usuario-agregar',
    component: RegistrarComponent
  },
  {
    path: 'usuario-editar/:id',
    component: EditarComponent
  },
  {
    path: 'acerca',
    component: AcercaComponent
  },
  { path: '',
    component: ListaComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AcercaComponent,
    ListaComponent,
    MostrarComponent,
    RegistrarComponent,
    EditarComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
