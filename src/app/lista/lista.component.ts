import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html'
})
export class ListaComponent implements OnInit{
  usuarios: any = [];

  constructor(public rest: UsersService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.getUsuarios();
  }

  getUsuarios() {
    this.usuarios = [];
    this.rest.getUsuarios().subscribe((data: {}) => {
      console.log(data);
      this.usuarios = data;
    });
  }

  delete(id) {
    this.rest.deleteUsuario(id)
      .subscribe(res => {
          this.getUsuarios();
        }, (err) => {
          console.log(err);
        }
      );
  }

  //End
}
