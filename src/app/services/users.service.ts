import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

const apiUrl = 'http://localhost:1337/';
const  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};


@Injectable({ providedIn: 'root' })
export class UsersService {

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  constructor(private http: HttpClient) {}

  // Rest Items Service: Read all REST Items
  getUsuarios(): Observable<any> {
    return this.http.get(apiUrl + 'usuarios').pipe(
      map(this.extractData)
    );
  }

  getUsuario(id): Observable<any> {
     return this.http.get(apiUrl + 'usuarios/' + id)
      .pipe(
        map(this.extractData)
      );
  }

  addUsuario (usuario): Observable<any> {
    console.log(usuario);
    return this.http.post<any>(apiUrl + 'usuarios', JSON.stringify(usuario),
      httpOptions).pipe(
        tap((usuario) => console.log(`Usuario Agregado w/ id=${usuario.id}`)),
        catchError(this.handleError<any>('addUsuario'))
      );
  }

  updateUsuario (id, usuario): Observable<any> {
    return this.http.put(apiUrl + 'usuarios/' + id, JSON.stringify(usuario),
      httpOptions).pipe(
        tap(_ => console.log(`Usuario Actualizado id=${id}`)),
        catchError(this.handleError<any>('updateUsuario'))
    );
  }

  deleteUsuario (id): Observable<any> {
    return this.http.delete<any>(apiUrl + 'usuarios/' + id, httpOptions).pipe(
      tap(_ => console.log(`Usuarios Eliminados id=${id}`)),
      catchError(this.handleError<any>('deleteUsuarios'))
    );
  }

  //Manejo de Excepciones
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} Error: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  //End
}
